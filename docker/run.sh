#!/bin/sh

cp school_logbook_api/settings/local_docker.py school_logbook_api/settings/local.py

pip install -U -r `[[ "$DJANGO_DEBUG" = "True" ]] && echo "requirements/development.txt" || echo "requirements/production.txt"`

exec gunicorn --config=/app/gunicorn.py school_logbook_api.wsgi
