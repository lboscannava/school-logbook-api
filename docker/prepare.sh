#!/bin/sh

set -ex

prepare () {
    cp school_logbook_api/settings/local_docker.py school_logbook_api/settings/local.py

    reqs="`[[ "$DJANGO_DEBUG" = "True" ]] && echo "requirements/development.txt" || echo "requirements/production.txt"`"

    pip install -U -r $reqs
}

prepare
