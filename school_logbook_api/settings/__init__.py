from .base import *  # NOQA

try:
    from .luis import *  # NOQA
except ImportError:
    import warnings
    warnings.warn('No local settings module found.')
